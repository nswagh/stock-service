##Overview

Stock service Spring Boot based CRUD REST API for Stocks. It follows Contract First approach using Swagger API spec and Swagger codegen generated interface and model. 

---
**API implementation details**

* This API uses H2 in memory DB for local and test profiles. For subsequent environments(sandbox, acceptance etc) MySql DB will be configured. MySql configuration is excluded from assignment as it will warrants dependency on external MySql. 
* StockController implements generated API interface and it uses generated model request and response classes. 
* Integration tests are added for testing happy and error scenarios 
* REST API is implemented using JDK 8, SpringBoot, JPA, H2, Swagger, JS, HTML. 
* At application start up, H2 DB is initiated with schema-h2.sql Stocks are loaded in h2 db at application startup.
* Stock domain is isolated from request and response DTOs using StockMapper.  
* StockRepository implements JPA PagingAndSortingRepository for CRUD and pagination 
---

**Instructions for running stock service**

Please follow the steps for building and running   

1. Open terminal and clone repository using "git clone https://nswagh@bitbucket.org/nswagh/stock-service.git"
2. Switch to stock-service directory using "cd stock-service"
3. Checkout review branch using "git fetch && git checkout review_branch"
4. Build project and run tests using 'mvn clean verify' 
5. Run the stock-service with local profile using "mvn spring-boot:run -Dspring.profiles.active=local"
6. Once application is up and running, API can be explored using Swagger UI http://127.0.0.1:8080/swagger-ui.html#/Stocks
7. There's also HTML and JS based UI to render stocks list. Please use http://127.0.0.1:8080/index.html to render stocks list. 


**REST endpoints details:**

Following is list of APIs and sample curl requests 
    
  * Get All stocks.
  
         Method : GET 
         URL    : http://127.0.0.1:8080/api/stocks
         CURL   : 
                  curl -i -X GET \
                    http://127.0.0.1:8080/api/stocks 
         
  * Get stocks for particular page and number of stocks on page.
  
         Method : GET 
         URL    : http://127.0.0.1:8080/api/stocks?page=2&size=5
         CURL   :
                  curl -i -X GET \
                    'http://127.0.0.1:8080/api/stocks?page=1&size=5'  
         
  * Get a stock by id.
    
         Method : GET 
         URL    : http://127.0.0.1:8080/api/stocks/{id}
         CURL   : 
                  curl -i -X GET \
                    http://127.0.0.1:8080/api/stocks/1 
         
   
  * Create a stock.
      
         Method : POST
         URL    : http://127.0.0.1:8080/api/stocks
         CURL   :
                  curl -i -X POST \
                    http://127.0.0.1:8080/api/stocks \
                    -H 'Content-Type: application/json' \
                    -d '{"stockName": "Morgan", "currentPrice":91}'  
  
  * Update an existing stock.
        
         Method : PUT
         URL    : http://127.0.0.1:8080/api/stocks/{id}
         CURL   :
                   curl -i -X PUT \
                     http://127.0.0.1:8080/api/stocks/1 \
                     -H 'Content-Type: application/json' \
                     -d '{"stockName": "Stan", "currentPrice":81}'
