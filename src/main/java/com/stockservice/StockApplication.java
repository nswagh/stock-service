package com.stockservice;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.stockservice.model.Stock;
import com.stockservice.service.StockService;

@SpringBootApplication
public class StockApplication implements ApplicationRunner {

	@Autowired
	StockService stockService;

	public static void main(String[] args) {
		SpringApplication.run(StockApplication.class, args);
	}

	@Override
	public void run(ApplicationArguments args) throws Exception {
		stockService.createStock(new Stock("HP", 13.0));
		stockService.createStock(new Stock("HCL", 33.0));
		stockService.createStock(new Stock("CG", 153.0));
		stockService.createStock(new Stock("Infy", 76.0));
		stockService.createStock(new Stock("Cogni", 13.0));
		stockService.createStock(new Stock("TCS", 99.0));
		stockService.createStock(new Stock("HSBC", 213.0));
		stockService.createStock(new Stock("Morgan", 215.0));
		stockService.createStock(new Stock("HP", 65.0));
		stockService.createStock(new Stock("Barclays", 90.0));
	}

}
