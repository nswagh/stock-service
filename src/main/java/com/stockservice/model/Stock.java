package com.stockservice.model;

import java.sql.Timestamp;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "Stock")
public class Stock implements java.io.Serializable {

	private static final long serialVersionUID = 4910225916550731446L;

	@Id
	@Column(name = "StockId", unique = true, nullable = false)
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long stockId;

	@Size(min = 2, max = 20)
	@Column(name = "StockName")
	private String stockName;

	@Column(name = "CurrentPrice")
	private Double currentPrice;

	@Column(name = "LastUpdatedOn")
	private Timestamp lastUpdate;

	public Stock(String stockName, Double currentPrice) {
		this.stockName = stockName;
		this.currentPrice = currentPrice;
		this.lastUpdate = new Timestamp(new Date().getTime());
	}

}
