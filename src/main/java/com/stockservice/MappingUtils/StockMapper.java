package com.stockservice.MappingUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import com.stockservice.model.Stock;
import com.stockservice.model.StockRequestBody;
import com.stockservice.model.StockResponseBody;

public class StockMapper {

	public static Stock convertStockRequestToStock(final StockRequestBody stockRequest) {
		Stock stock = new Stock();

		stock.setStockName(stockRequest.getStockName());
		stock.setCurrentPrice(stockRequest.getCurrentPrice());
		stock.setLastUpdate(new Timestamp(new Date().getTime()));
		return stock;
	}

	public static List<StockResponseBody> convertStockListToResponse(List<Stock> stockList) {
		return stockList.stream().map(StockMapper::convertStockToStockResponse).collect(Collectors.toList());
	}

	public static StockResponseBody convertStockToStockResponse(final Stock stock) {
		StockResponseBody stockResponse = new StockResponseBody();
		stockResponse.setStockId(stock.getStockId().intValue());
		stockResponse.setStockName(stock.getStockName());
		stockResponse.setCurrentPrice(stock.getCurrentPrice());
		stockResponse.setLastUpdate(getFormattedDateTime(stock.getLastUpdate()));
		return stockResponse;
	}

	public static String getFormattedDateTime(Timestamp ts) {
		Date date = new Date();
		date.setTime(ts.getTime());
		String formattedDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
		return formattedDate;
	}
}
