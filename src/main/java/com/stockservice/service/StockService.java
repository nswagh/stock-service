package com.stockservice.service;

import java.util.List;

import com.stockservice.model.Stock;

public interface StockService {

	Stock createStock(Stock stock);

	Stock updateStock(Integer stockId, Stock stock);

	Stock getStockById(Integer id);

	List<Stock> getStocks(int pageNumber, int recordsPerPage);

}