package com.stockservice.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.stockservice.exception.StockNotFoundException;
import com.stockservice.model.Stock;
import com.stockservice.repository.StockRepository;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class StockServiceImpl implements StockService {

	@Autowired
	private StockRepository stockRepository;

	@Override
	public Stock createStock(Stock stock) {
		log.info("Creating stock with Name: {}" + stock.getStockName());
		return stockRepository.save(stock);
	}

	@Override
	public Stock updateStock(Integer stockId, Stock stock) {
		Stock savedStock = getStockById(stockId);
		savedStock.setStockName(stock.getStockName());
		savedStock.setCurrentPrice(stock.getCurrentPrice());
		savedStock.setLastUpdate(new Timestamp(new Date().getTime()));
		log.info("Updating stock with Name: {}" + stock.getStockName() + "with Price: {}" + stock.getCurrentPrice());
		return stockRepository.save(savedStock);
	}

	@Override
	public Stock getStockById(Integer id) {
		log.info("Retrieving stock with Id: {}" + id);
		return stockRepository.findById(Long.valueOf(id))
				.orElseThrow(() -> new StockNotFoundException("Stock with Id: " + id + " does not exist"));
	}

	@Override
	public List<Stock> getStocks(int pageNumber, int recordsPerPage) {
		log.info("Retrieving stocks with Page: {}" + pageNumber + " and size: {}" + recordsPerPage);
		if (pageNumber > 0) {
			pageNumber--;
		}
		Page<Stock> stocks = stockRepository.findAll(PageRequest.of(pageNumber, recordsPerPage));
		return stocks.getContent();
	}

}
