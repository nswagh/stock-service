package com.stockservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.stockservice.MappingUtils.StockMapper;
import com.stockservice.api.StocksApi;
import com.stockservice.model.StockRequestBody;
import com.stockservice.model.StockResponseBody;
import com.stockservice.service.StockService;

@Controller
@RequestMapping("api/")
public class StockController implements StocksApi {

	@Autowired
	StockService stockService;

	@Override
	public ResponseEntity<StockResponseBody> createStock(@Valid @RequestBody StockRequestBody stockRequest) {
		return ResponseEntity.ok(StockMapper.convertStockToStockResponse(
				stockService.createStock(StockMapper.convertStockRequestToStock(stockRequest))));
	}

	@Override
	public ResponseEntity<StockResponseBody> getStockById(@PathVariable("stockId") Integer stockId) {
		return ResponseEntity.ok(StockMapper.convertStockToStockResponse(stockService.getStockById(stockId)));
	}

	@Override
	@GetMapping(value = "stocks")
	public ResponseEntity<List<StockResponseBody>> getStocks(
			@Valid @RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
			@Valid @RequestParam(value = "size", required = false, defaultValue = "20") Integer size) {
		return ResponseEntity.ok(StockMapper.convertStockListToResponse(stockService.getStocks(page, size)));
	}

	@Override
	public ResponseEntity<Void> updateStock(@PathVariable("stockId") Integer stockId,
			@Valid @RequestBody StockRequestBody updateStock) {
		stockService.updateStock(stockId, StockMapper.convertStockRequestToStock(updateStock));
		return ResponseEntity.ok().build();
	}
}
