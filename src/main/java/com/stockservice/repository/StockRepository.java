package com.stockservice.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.stockservice.model.Stock;

@Repository
public interface StockRepository extends PagingAndSortingRepository<Stock, Long> {

}
