-- Initialize table with some record stocks
INSERT INTO Stock(stockId, StockName, CurrentPrice, LastUpdatedOn)VALUES (1,'Mankind', 50,CURRENT_TIMESTAMP);
INSERT INTO Stock(stockId, StockName, CurrentPrice, LastUpdatedOn)VALUES (2,'HP', 65, CURRENT_TIMESTAMP);
INSERT INTO Stock(stockId, StockName, CurrentPrice, LastUpdatedOn)VALUES (3,'Oracle', 80,CURRENT_TIMESTAMP);

