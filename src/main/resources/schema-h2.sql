drop table if exists Stock;
create table Stock
( 
  StockId              INTEGER PRIMARY KEY,
  StockName            VARCHAR(256),
  CurrentPrice         DOUBLE,
  LastUpdatedOn 	   TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

