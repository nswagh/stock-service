$(document).ready(function() {
	var settings = {
		"async" : true,
		"crossDomain" : true,
		"url" : "http://127.0.0.1:8080/api/stocks",
		"method" : "GET"
	}

	$.ajax(settings).done(function(response) {
		drawTableData(response);
	});

	function drawTableData(data) {
		for (var item = 0; item < data.length; item++) {
			renderEachRow(data[item]);
		}
	}

	function renderEachRow(rowData) {
		var row = $("<tr />")
		$("#stockTable").append(row);
		row.append($("<td>" + rowData.stockId + "</td>"));
		row.append($("<td>" + rowData.stockName + "</td>"));
		row.append($("<td>" + rowData.currentPrice + "</td>"));
		row.append($("<td>" + rowData.lastUpdate + "</td>"));
	}
});
