swagger: '2.0'
info:
   version: 1.0.0
   title: Stock API
   description: API to perform Create, Update, Get, GetAll operations on stock
schemes:
   - http
host: stockhost
basePath: /api
tags:
-  name: Stocks
   description: Stock operations
paths:
   /stocks:
      get:
         description: Get all stocks
         operationId: getStocks
         tags:
         - Stocks
         produces:
         - application/json
         parameters:
         -  name: page
            description: page number to be retrieved from available pages
            in: query
            type: integer
            default: 1
         -  name: size
            description: number stocks to be retrieved for every page
            in: query
            type: integer
            default: 20
         responses:
            200:
               description: List of stocks retrieved successfully
               schema:
                  type: array
                  items:
                     $ref: '#/definitions/StockResponseBody'
            204:
               description: No content found
               schema:
                  $ref: '#/definitions/Error'
      post:
         description: create new stock
         operationId: createStock
         tags:
         - Stocks
         consumes:
         - application/json
         parameters:
         -  name: stock-request
            in: body
            required: true
            schema:
               $ref: '#/definitions/StockRequestBody'
         responses:
            200:
               description: Stock created successfully
               schema:
                  $ref: '#/definitions/StockResponseBody'
            404:
               description: Content not found
               schema:
                  $ref: '#/definitions/Error'
            500:
               description: Server Error
               schema:
                  $ref: '#/definitions/Error'
   /stocks/{stockId}:
      get:
         description: Get Stock by Id
         operationId: getStockById
         tags:
         - Stocks
         produces:
         - application/json
         parameters:
         -  name: stockId
            type: integer
            in: path
            description: stock Id
            required: true
         responses:
            200:
               description: Stock retrieved successfully
               schema:
                  $ref: '#/definitions/StockResponseBody'
            404:
               description: No data found
               schema:
                  $ref: '#/definitions/Error'
            500:
               description: Server Error
               schema:
                  $ref: '#/definitions/Error'
      put:
         description: Update stock
         operationId: updateStock
         tags:
         - Stocks
         consumes:
         - application/json
         produces:
         - application/json
         parameters:
         -  name: stockId
            type: integer
            in: path
            description: stock Id
            required: true
         -  name: update-stock
            in: body
            required: true
            schema:
               $ref: '#/definitions/StockRequestBody'
         responses:
            200:
               description: Stock is successfully updated
            404:
               description: Content not found
               schema:
                  $ref: '#/definitions/Error'
            500:
               description: Server Error
               schema:
                  $ref: '#/definitions/Error'
definitions:
   StockRequestBody:
      type: object
      properties:
         stockName:
            description: Name for stock
            type: string
            minLength: 1
            maxLength: 200
         currentPrice:
            description: Price for stock
            type: number
            format: double
            default: 1
      required:
      - stockName
      - currentPrice
   StockResponseBody:
      type: object
      properties:
         stockId:
            description: Stock Id
            type: integer
         stockName:
            description: Name for stock
            type: string
            minLength: 1
            maxLength: 200
         currentPrice:
            description: Price for stock
            type: number
            format: double
            default: 1
         lastUpdate:
            description: Last updated timestamp
            type: string
      required:
      - stockId
      - stockName
      - currentPrice
   Error:
      type: object
      properties:
         error:
            description: Error code
            type: string
         error_description:
            description: Error description
            type: string
      required:
      - error
      - error_description