package com.stockservice;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Map;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.stockservice.repository.StockRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class StockServiceTests {

	private static final String BASE_PATH = "/api/stocks/";

	private ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private StockRepository stockRepository;

	@Before
	public void setup() throws Exception {
		stockRepository.deleteAll();
	}

	@Test
	public void createStockShouldSucced() throws Exception {
		mockMvc.perform(post(BASE_PATH).contentType(MediaType.APPLICATION_JSON_UTF8)
				.content("{\"stockName\":\"DummyStock\",\"currentPrice\":23}")).andExpect(status().isOk()).andReturn();
	}

	@Test
	public void createStockShouldReturnInBadRequest() throws Exception {
		mockMvc.perform(post(BASE_PATH).contentType(MediaType.APPLICATION_JSON_UTF8)
				.content("{\"stockId\":\"DummyStock\",\"currentPrice\":null,\"currentPrice\":50}"))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void getAllStocksShouldSuccess() throws Exception {
		String stockUri = BASE_PATH;
		mockMvc.perform(get(stockUri).contentType(MediaType.APPLICATION_JSON_UTF8)).andExpect(status().isOk());
	}

	@Test
	public void getStockShouldSuccess() throws Exception {
		MvcResult mvcResult = mockMvc.perform(post(BASE_PATH).contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"stockName\":\"DummyStock\",\"currentPrice\":45}"))
				.andExpect(status().isOk()).andReturn();
		String responseJson = mvcResult.getResponse().getContentAsString();
		Map<String, Object> map = mapper.readValue(responseJson, Map.class);
		Integer stockId = (Integer) map.get("stockId");
		String stockByIdUri = BASE_PATH + stockId;
		mockMvc.perform(get(stockByIdUri)).andExpect(status().isOk());
	}

	@Test
	public void getStocksShouldReturnNoDataFound() throws Exception {
		mockMvc.perform(get("/api/stocks/123").contentType(MediaType.APPLICATION_JSON_UTF8)
				.content("{\"stockId\":\"123\",\"stockName\":\"DummyStock\",\"currentPrice\":80}"))
				.andExpect(status().isNotFound());
	}

	@Test
	public void updateStockShouldSucced() throws Exception {
		MvcResult mvcResult = mockMvc.perform(post(BASE_PATH).contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"stockName\":\"DummyStock\",\"currentPrice\":35}"))
				.andExpect(status().isOk()).andReturn();
		String responseJson = mvcResult.getResponse().getContentAsString();
		Map<String, Object> map = mapper.readValue(responseJson, Map.class);
		Integer stockId = (Integer) map.get("stockId");
		String stockByIdUri = BASE_PATH + stockId;
		mockMvc.perform(put(stockByIdUri).contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"stockName\":\"TDummyStockNew\",\"currentPrice\":29}"))
		.andExpect(status().isOk()).andReturn();
		mockMvc.perform(get(stockByIdUri)).andExpect(status().isOk()).andExpect(jsonPath("$.currentPrice").value("29.0"));
	}

	@Test
	public void updateStockShouldReturnInBadRequest() throws Exception {
		MvcResult mvcResult = mockMvc.perform(post(BASE_PATH).contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"stockName\":\"DummyStock\",\"currentPrice\":54}"))
				.andExpect(status().isOk()).andReturn();
		String responseJson = mvcResult.getResponse().getContentAsString();
		Map<String, Object> map = mapper.readValue(responseJson, Map.class);
		Integer stockId = (Integer) map.get("stockId");
		String stockByIdUri = BASE_PATH + stockId;
		mockMvc.perform(put(stockByIdUri).contentType(MediaType.APPLICATION_JSON_UTF8).content("{\"stockId\":\"25\",\"stockName\":\"DummyStock\",\"currentPrice\":null}"))
				.andExpect(status().isBadRequest());
	}

	@Test
	public void updateStockShouldReturnNoDataFound() throws Exception {
		mockMvc.perform(put("/api/stocks/123").contentType(MediaType.APPLICATION_JSON_UTF8)
				.content("{\"stockId\":\"34\",\"stockName\":\"DummyStock\",\"currentPrice\":95}"))
				.andExpect(status().isNotFound());
	}

}
